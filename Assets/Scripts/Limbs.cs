﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Limbs : MonoBehaviour
{
    [SerializeField] Limbs[] childLimbs;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetHit()
    {
        if(childLimbs.Length < 0)
        {
            foreach(Limbs limbs in childLimbs)
            {
                if(limbs != null)
                {
                    limbs.GetHit();
                }
            }
        }

        transform.localScale = Vector3.zero;

        Destroy(this);
    }


}
