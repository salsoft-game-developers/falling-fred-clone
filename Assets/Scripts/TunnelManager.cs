﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TunnelManager : MonoBehaviour
{
    #region Singleton Class: TunnelManager

    public static TunnelManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    #endregion

    //public GameObject TunnelPrefab;
    //public Vector3 instantPos = new Vector3(0, -251, 0);
    //Vector3 tunnelPos;

    [SerializeField] GameObject[] tunelPrefabs;

 //[SerializeField] GameObject planePrefab;

    Transform playerTransform;
    float spawnY = 0f;
    float tunelLength = 100f;

    //float safeZone = 150f;

    int amountOfTunels = 4;
    int lastPrefabIndex = 0;

    private List<GameObject> activeTunel;

    // Start is called before the first frame update

    void Start()
    {

        activeTunel = new List<GameObject>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        for(int i = 0; i < amountOfTunels; i++)
        {
            if(i< 2) { SpawnTunel(0); }
            else { SpawnTunel(); }
        }

       
    }

    // Update is called once per frame
    void Update()
    {

        //if (playerTransform.position.y - safeZone  < (spawnY - amountOfTunels * tunelLength)) 
        //{
        //    SpawnTunel();
        //    //DeleteTunel();
        //}


        if (-playerTransform.position.y > (-activeTunel[activeTunel.Count-2].gameObject.transform.position.y) )
        {
            SpawnTunel();
            DeleteTunel();
        }

    }


    //private void OnTriggerEnter(Collider other)
    //{
    //    if(other.tag == "Player")
    //   {
    //        //Instantiate(TunnelPrefab, new Vector3(0,-1000,0), Quaternion.identity);
    //        Instantiate(TunnelPrefab, tunnelPos + instantPos, Quaternion.identity);
    //   }
    //}


    void SpawnTunel(int prefabIndex = -1)
    {
        GameObject Go;
        if(prefabIndex == -1)
        {
            Go = Instantiate(tunelPrefabs[RandomPrefabIndex()]) as GameObject;
        }
        else
        {
            Go = Instantiate(tunelPrefabs[prefabIndex]) as GameObject;
        }
        
        Go.transform.SetParent(transform);
        Go.transform.position = Vector3.down * spawnY;
        spawnY += tunelLength;
        activeTunel.Add(Go);
    }

    void DeleteTunel()
    {
        Destroy(activeTunel[0]);
        activeTunel.RemoveAt(0);


    }

    private int RandomPrefabIndex()
    {
        if(tunelPrefabs.Length <= 1) { return 0; }

        int randomIndex = lastPrefabIndex;
        while(randomIndex == lastPrefabIndex)
        {
            randomIndex = Random.Range(0, tunelPrefabs.Length);
        }

        lastPrefabIndex = randomIndex;
        return randomIndex;
    }

    public void ActivePlane()
    {
        activeTunel[activeTunel.Count-1].gameObject.transform.GetChild(1).gameObject.SetActive(true);
    } 

}
