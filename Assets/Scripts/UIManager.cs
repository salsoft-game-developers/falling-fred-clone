﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    #region Singleton Class: UIManager

    public static UIManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    #endregion

    public GameObject gameOverScreen;
    public Text scoreText;
    public Text highScoreTxt;
    public float score;
    float fallingSpeed = 0.05f;

    [SerializeField] ParticleSystem speedEffect;

    // Start is called before the first frame update

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpeedEffect()
    {
        if (((int)score) != 0 && ((int)score) % 101 == 0)
        {
            speedEffect.Play();
            StartCoroutine(SpeedEffectDelay());
        }
    }

    IEnumerator SpeedEffectDelay()
    {
        yield return new WaitForSeconds(5);
        speedEffect.Stop();
    }

    public void HighScoreFunction(float position)
    {
        score = (-position) * fallingSpeed;

        scoreText.text = "Your Score: " + score.ToString("0");
    }

    public void GameOverScreen()
    {
        gameOverScreen.SetActive(true);
        highScoreTxt.text = "High Score: " + score.ToString("0");
    }


    public void RestartGame()
    {
        SceneManager.LoadScene(0); // loads current scene
    }

   
}
