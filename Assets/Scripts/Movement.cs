﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movement : MonoBehaviour
{

   //[SerializeField] float speed = 5f;
    
    [SerializeField] float movingForce = 100f;
    [SerializeField] float playerHealth = 10f;
    [SerializeField] Slider HealthBar;
    [SerializeField] GameObject bloodEffect;
    [SerializeField] GameObject bloodSplash;
    [SerializeField] ParticleSystem shockWave;

    Limbs[] allLimbs;
    Rigidbody rb;

    public bool gamePlaying;

    //Limbs destroyedLimbs;
    //int index;

    // Start is called before the first frame update

    void Start()
    {
        gamePlaying = true;
        rb = GetComponent<Rigidbody>();
        
    }

    // Update is called once per frame
    
    void Update()
    {
        
        if (gamePlaying == true)
        {
            CharacterMovemnent();
            UIManager.Instance.SpeedEffect();
            if (playerHealth <= 0)
            {
                StartCoroutine(DeathSequence());
            }
                
        }

        if(gamePlaying == false) 
        {
            
            return;

        }
    }

    private void CharacterMovemnent()
    {
#if UNITY_EDITOR

        //get the Input from Horizontal axis

        float horizontalInput = Input.GetAxis("Horizontal");

        //get the Input from Vertical axis
        float verticalInput = Input.GetAxis("Vertical");

        //update the position
        //transform.position = transform.position + new Vector3(horizontalInput * speed * Time.deltaTime,0, verticalInput * speed * Time.deltaTime);
        rb.AddForce( horizontalInput * movingForce, 0, verticalInput * movingForce);


#else

        //transform.position = transform.position + new Vector3(Input.acceleration.x * force, 0, Input.acceleration.z * force);
        Vector3 acc = Input.acceleration;
        rb.AddForce(acc.x * movingForce , 0, acc.y * movingForce);

#endif
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Obstacles")
        {

            bloodEffect.SetActive(true);
            StartCoroutine(BloodSplash());

            playerHealth -= 1.5f;
            HealthBar.value = playerHealth;

            GameObject[] bodyParts = GameObject.FindGameObjectsWithTag("Limbs");
            allLimbs = new Limbs[bodyParts.Length];

            //for (int i=0; i<= allLimbs.Length; i++)
            //{
                //allLimbs[i] = bodyParts[i].GetComponent<Limbs>();
                //index = Random.Range(0, i);
                //destroyedLimbs = allLimbs[index];
                //destroyedLimbs.GetHit();
            //}

            int i = Random.Range(0, bodyParts.Length-1);
            allLimbs[i] = bodyParts[i].GetComponent<Limbs>();
            allLimbs[i].GetHit();

            if(playerHealth < 1.5f)
            {
                Limbs Head = GameObject.FindGameObjectWithTag("Head").GetComponent<Limbs>();
                Head.GetHit();
                playerHealth = 0.75f;
                HealthBar.value = playerHealth;
            }

        }

        if(collision.gameObject.tag == "Wall")
        {
            playerHealth -= 0.25f;
            HealthBar.value = playerHealth;
        }

    }

    private void OnCollisionStay(Collision collision)
    {
        if(collision.gameObject.tag == "Obstacles" || collision.gameObject.tag == "Wall")
        {
            bloodEffect.SetActive(true);
        }

    }

    private void OnCollisionExit(Collision collision)
    {
        bloodEffect.SetActive(false);
   
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "DeadEnd")
        {
            shockWave.Play();
        }
    }

    //private void OnTriggerStay(Collider other)
    //{
    //    if(other.gameObject.tag == "DeadEnd")
    //    {
    //        shockWave.Pause();
    //    }
    //}

    public IEnumerator BloodSplash()
    {
        bloodSplash.SetActive(true);
        yield return new WaitForSeconds(2);
        bloodSplash.SetActive(false);
    }
 
    public IEnumerator DeathSequence()
    {

        //Debug.Log("health is 0");
        gamePlaying = false;
        TunnelManager.Instance.ActivePlane();
        yield return StartCoroutine(WaitASec(10f));
        UIManager.Instance.GameOverScreen();
        GetComponent<Rigidbody>().isKinematic = true;
        
    }

    public IEnumerator WaitASec(float timeToWait)
    {
        yield return new WaitForSeconds(timeToWait);
    }

    //public void PlayGame()
    //{
    //    gamePlaying = true;
        
    //} 
    //public void StopGame()
    //{
    //    gamePlaying = false;
        
    //}

}












//if (Input.GetKeyDown(KeyCode.A))
//{
//    Debug.Log("A");
//    rb.AddForce(-transform.right * force * Time.deltaTime);
//}

//if(Input.GetKeyDown(KeyCode.D))
//{
//    rb.AddForce(transform.right * force * Time.deltaTime);
//}

//if(Input.GetKeyDown(KeyCode.W))
//{
//    rb.AddForce(new Vector3(0,0,50) * force * Time.deltaTime);
//}

//if(Input.GetKeyDown(KeyCode.S))
//{
//    rb.AddForce(new Vector3(0,0,-50) * force * Time.deltaTime);
//}